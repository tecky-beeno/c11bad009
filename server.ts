import express from 'express';
import Knex from 'knex';
import { Server } from 'http';
import { promisify } from 'util';
let knexConfigs = require('./knexfile');
import fs from 'fs';

let version = JSON.parse(fs.readFileSync('package.json').toString()).version;

let mode = process.env.NODE_ENV || 'development';

let knex = Knex(knexConfigs[mode]);

let app = express();

async function incrementCounter() {
  // `update visitor set count = count + 1`
  await knex.transaction(async txn => {
    let row = await txn
      .select('*')
      .from('visitor')
      .first();
    row.count++;
    await txn('visitor').update(row);
  });
}

app.get('/version', (req, res) => {
  res.json({ version });
});

app.get('/count', async (req, res) => {
  try {
    let row = await knex
      .select('*')
      .from('visitor')
      .first();
    res.json(row);
  } catch (error) {
    res.status(500).json({ error: error.toString() });
  }
});

app.use((req, res, next) => {
  // console.log(req.method, req.url, req.connection.address());
  incrementCounter();
  next();
});

app.use(express.static('public'));

export async function startServer(port: number): Promise<Server> {
  return new Promise((resolve, reject) => {
    let server = app.listen(port, () => {
      console.log('listening on port', port);
      resolve(server);
    });
  });
}

export async function stopServer(server: Server) {
  let close = promisify(server.close.bind(server));
  await close();
  await knex.destroy();
}
